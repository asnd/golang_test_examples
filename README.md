# golang test examples

# benchmark

### простой запуск benchmark
```go
go test -bench=. <filename>_test.go
```
**вывод**
`testing: warning: no tests to run
PASS
BenchmarkSample 10000000 206 ns/op
ok command-line-arguments 2.274s`
***
### флаг -benchmem - позволит тестировать потребление памяти и количество аллокаций памяти
```go
go test -bench=. -benchmem <filename>_test.go
```
**вывод**
`PASS
BenchmarkSample 10000000 208 ns/op 32 B/op 2 allocs/op`
***
### вывод результатов в файл
```go
go test -bench=. -benchmem <filename>_test.go > <filename>.txt
```
***
### сравнение результатов
Мы можем сравнить эти результаты с помощью утилиты benchcmp. Вы можете установить ее, выполнив команду ``` go get golang.org/x/tools/cmd/benchcmp. ```
***

### запись cpu и memory профилей в файл во время выполнения бенчмарков:
```go
go test -bench=. -benchmem -cpuprofile=cpu.out -memprofile=mem.out <filename>_test.go
```
### визуализировать профиль 
```go
go tool pprof -svg ./perftest00.test ./cpu.out > cpu.svg ./mem.out > mem.svg
```

## пример выгрузки профиля в файл 
```go
go test -bench=. -benchmem -cpuprofile=cpu.out -memprofile=mem.out third_test.go
```

# tests












