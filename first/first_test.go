package first

import (
	"strconv"
	"sync"
	"testing"
)

type Set struct {
	set map[interface{}]struct{}
	mu  sync.Mutex
}

func CreateSet() Set {
	return Set{set: make(map[interface{}]struct{})}
}

func (s *Set) Add(x interface{}) {
	s.mu.Lock()
	s.set[x] = struct{}{}
	s.mu.Unlock()
}

func (s *Set) Delete(x interface{}) {
	s.mu.Lock()
	delete(s.set, x)
	s.mu.Unlock()
}

// start test
// go test -bench=. controller_test.go
func BenchmarkSetDelete(b *testing.B) {
	var testSet []string
	for i := 0; i < 1024; i++ {
		testSet = append(testSet, strconv.Itoa(i))
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		b.StopTimer() // останавливает счетчик времени
		set := Set{set: make(map[interface{}]struct{})}
		for _, elem := range testSet {
			set.Add(elem)
		}
		b.StartTimer() // запускаем счетчик времени
		for _, elem := range testSet {
			set.Delete(elem)
		}
	}
}
