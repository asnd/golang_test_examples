package third

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"testing"
	"time"

	config "gitlab8.alx/pcs/common/config.git"
	"gitlab8.alx/pcs/common/go-connectors.git/aerospike"
	nats_stan "gitlab8.alx/pcs/common/go-connectors.git/nats-stan"
	rabbit_cony "gitlab8.alx/pcs/common/go-connectors.git/rabbit-cony"
	"gopkg.in/yaml.v2"
)

// Config structure in which parsing a config
type Config struct {
	App struct {
		CheckConnectionsInterval time.Duration `yaml:"check_connections_interval"`
		CTXTime                  time.Duration `yaml:"ctx_time"`
		ProfilerHost             string        `yaml:"prfiler_host"`
		ProfilerPort             string        `yaml:"profiler_port"`
		Logger                   struct {
			LogService struct {
				Level string `yaml:"level"`
			} `yaml:"log_service"`
		} `yaml:"logger"`
	} `yaml:"app"`

	Connections struct {
		AS     aerospike.Config   `yaml:"aerospike"`
		Nats   nats_stan.Config   `yaml:"nats"`
		Rabbit rabbit_cony.Config `yaml:"amqp"`
	} `yaml:"connections"`
}

// LoadConfig parsing config
func LoadConfig(name string) (*Config, error) {
	var cfg Config
	var err error
	if err = config.ParseFile(name, &cfg); err != nil {
		return nil, fmt.Errorf("config loading from file error: %s", err)
	}

	if err = cfg.Validate(); err != nil {
		return nil, fmt.Errorf("config validation error: %s", err)
	}
	return &cfg, nil
}

// Validate config validation
func (cfg *Config) Validate() error {
	var err error

	if err = cfg.Connections.Nats.Validate(); err != nil {
		return err
	}

	if err = cfg.Connections.AS.Validate(); err != nil {
		return err
	}

	if err = cfg.Connections.Rabbit.Validate(); err != nil {
		return err
	}

	return nil
}

// PrintConfig debug for config output that is in config will work if run with flag -p ex: go run main.go -p config.yaml
func PrintConfig(path string) error {
	var err error
	filename, err := filepath.Abs(path)
	if err != nil {
		return err
	}
	yamlFile, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}
	var configData Config
	err = yaml.Unmarshal(yamlFile, &configData)
	if err != nil {
		return err
	}
	// fmt.Printf("Value: %+v\n", configData)
	return nil
}

// go test -bench=. -benchmem
func BenchmarkPrintConfig(b *testing.B) {
	for i := 0; i < b.N; i++ {
		PrintConfig("config.yaml")
	}
}

// // go test -bench=. -benchmem
// func BenchmarkLoadConfig(b *testing.B) {
// 	for i := 0; i < b.N; i++ {
// 		LoadConfig("config.yaml")
// 	}
// }
